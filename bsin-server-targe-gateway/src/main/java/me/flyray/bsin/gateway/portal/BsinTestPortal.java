package me.flyray.bsin.gateway.portal;


import com.alipay.sofa.rpc.api.GenericService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import lombok.extern.log4j.Log4j2;
import me.flyray.bsin.gateway.context.BsinContextBuilder;
import me.flyray.bsin.gateway.utils.BsinTestServiceInvokeUtil;

@Log4j2
@RestController
public class BsinTestPortal {

    private static ConcurrentHashMap<String, GenericService> concurrentHashMap = new ConcurrentHashMap<String, GenericService>();
    @Autowired
    public BsinContextBuilder bsinContextBuilder;

    @Autowired
    public BsinTestServiceInvokeUtil bsinTestServiceInvokeUtil;

    /**
     * http请求入口
     * 1、拼装请求参数
     * 2、唤起服务: 泛化 编排
     * 3、拼装响应报文
     *
     * @param req
     * @return
     */
    @PostMapping("/test-gateway")
    public void portal(@RequestBody Map<String, Object> req) throws IOException {


        /*ConsumerConfig<HelloService> consumerConfig = new ConsumerConfig<HelloService>()
                .setInterfaceId(HelloService.class.getName()) // 指定接口
                .setProtocol("bolt") // 指定协议
                .setDirectUrl("bolt://192.168.0.117:12290"); // 指定直连地址
        // 生成代理类
        HelloService helloService = consumerConfig.refer();*/
        Map<String, Object> resultData = new HashMap<String, Object>();
        resultData.put("fileUrl", "https://ipfs.flyray.me/ipfs/");
        resultData.put("fileHash", "11");
        bsinTestServiceInvokeUtil.genericInvoke("HelloService", "add", resultData);

        /*while (true) {
            helloService.add(resultData);
            try {
                Thread.sleep(2000);
            } catch (Exception e) {
            }
        }*/
    }


}
