import React, { ChangeEvent, FC, useState } from 'react';
import graphic2 from '@/assets/picture/graphic2.svg';
import { connect, Dispatch, Loading, AppsState, history } from 'umi';
import { message, Button, Select } from 'antd';
import { merchantLogin, getTenantList, getTenantBaseApp } from '../../services/login';
import { userRegister } from '../../services/register';
import {
  setLocalStorageInfo,
  setSessionStorageInfo,
} from '@/utils/localStorageInfo';
import styles from './iofrm-theme5.less';
import { hex_md5 } from '../../utils/md5';

const { Option } = Select;

interface PageProps {
  apps: AppsState;
  dispatch: Dispatch;
}

const Login2: FC<PageProps> = ({ apps, dispatch }) => {

  React.useEffect(() => {
    getAllTenant();
  }, []);

  // 租户列表
  const [tenantList, setTenantList] = useState<[]>();
  // 租户岗位
  const [tenantBaseApp, setTenantBaseApp] = useState<{}>();
  // 选中的租户
  const [tenantId, setTenantId] = useState<string>();
  // 选中的租户的默认岗位
  const [postId, setPostId] = useState<string>();

  // 获取所有租户
  const getAllTenant = async () => {
    let res = await getTenantList({});
    if (res && res.code === '000000') {
      setTenantList(res.data);
    } else {
      message.error('获取租户列表失败');
    }
  };


  // 登录、注册切换
  const [pagesWitching, setPagesWitching] = React.useState('login');
  // 登录表单值
  const [loginState, setLoginState] = React.useState({
    tenantId: '',
    username: '',
    password: '',
  });
  // 注册表单
  const [registerState, setRegisterState] = React.useState({
    registerUsername: '',
    registerPassword: '',
    phone: '',
  });

  // 按钮点击动画
  const [loadings, setLoadings] = React.useState(false);

  // 登录选择租户
  const handleChange = (value: string) => {
    setLoginState({
      ...loginState,
      tenantId: value,
    });
  };

  // 登录表单值变化调用
  const onChange = (event: ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value;
    const name = event.target.name;
    setLoginState({
      ...loginState,
      [name]: value,
    });
  };

  // 选择租户
  function onChangeTenant(value: string) {
    setTenantId(value);
  }

  // 选择租户
  function onChangePost(value: string) {
    setPostId(value);
  }

  // 登录按钮调用
  const login = async (event: any) => {
    if (!tenantId) {
      return message.info('请选择节点');
    }
    // 如果用户名和密码为空，则提示
    if (!loginState.username) {
      return message.info('请输入用户名');
    }
    if (!loginState.password) {
      return message.info('请输入密码');
    }
    setLoadings(true);
    let res = await merchantLogin({
      ...loginState,
      tenantId: tenantId,
      password: hex_md5(loginState.password),
    });
    if (res) {
      if(!res.data?.sysUser){
        setLocalStorageInfo('userInformation', res.data?.customerInfo);
      }else{
        setLocalStorageInfo('userInformation', res.data?.sysUser);
      }
      setLocalStorageInfo('merchantInfo', res.data?.merchantInfo);
      setLocalStorageInfo('customerInfo', res.data?.customerInfo);
      setSessionStorageInfo('token', { token: res.data?.token });
      // 查询用户拥有的应用
      dispatch({
        type: 'apps/getUserApps',
        payload: {
          current: 1,
          pageSize: 8,
          userType: 2
        },
      });
      message.success('登录成功！');

      getTenantBaseApp({tenantId}).then((res)=>{
        console.log(res)
        setTenantBaseApp(res?.data)
        history.push('/workplace');
        // apps.appList.length === 1
        // ? history.push(tenantBaseApp?.appCode)
        // : history.push('/workplace');
      })

      // setTimeout(() => {
      //   if(apps.appList.length === 1){
      //     history.push(apps.appList[0].url)
      //   }else if(apps.appList.length < 1){
      //     // history.push(tenantBaseApp?.appCode)
      //   }
      //   // TODO 查询当前登录商户所属租户的默认应用首页
      //   setLoadings(false);
      // }, 500);
    }
    setLoadings(false);
  };

  // 注册表单值变化调用
  const onRegisterChange = (event: ChangeEvent<HTMLInputElement>) => {

    const value = event.target.value;
    const name = event.target.name;
    setRegisterState({
      ...registerState,
      [name]: value,
    });
  };

  // 注册按钮调用
  const register = async (event: any) => {
    if (!tenantId) {
      return message.info('请选择节点');
    }
    const {
      registerUsername: username,
      registerPassword: password,
      phone,
    } = registerState;
    // 如果用户名和密码为空，则提示

    if (!username) {
      return message.info('请输入用户名！');
    }
    if (!password) {
      return message.info('请输入密码！');
    }
    if (!phone) {
      return message.info('请输入手机号！');
    }
    let telTest =
      /^1(3[0-9]|4[01456879]|5[0-35-9]|6[2567]|7[0-8]|8[0-9]|9[0-35-9])\d{8}$/;
    if (!telTest.test(phone)) {
      return message.info('请输入正确的手机号！');
    }
    setLoadings(true);
    // console.log(hex_md5(registerState.password));

    let res = await userRegister({
      username,
      phone,
      tenantAppType: process.env.jiujiu.tenantAppType,
      //tenantId: process.env.jiujiu.tenantId,
      tenantId: tenantId,
      postId: postId,
      password: hex_md5(password),
    });
    if (res?.code === '000000') {
      setLoginState({
        ...loginState,
        username,
        password,
      });
      message.success('恭喜您，注册成功');
      setPagesWitching('login');
    }
    setLoadings(false);
  };

  return (
    <div className={styles['form-body']}>
      <div className={styles['row']}>
        <div className={styles['img-holder']}>
          <img src={graphic2} />
        </div>
        <div className={styles['form-holder']}>
          <div className={styles['form-content']}>
            <h3>bigan</h3>
            <p>一站式的Web3.0品牌构建服务网络</p>
            {pagesWitching === 'register' ? (
              <form>
                <Select
                  bordered={false}
                  className={styles['form-control']}
                  style={{
                    // marginTop: 20,
                    backgroundColor: '#eee',
                    border: 'none',
                    padding: '7px 0px',
                    margin: '14px 0',
                    width: '100%',
                    textAlign: 'left',
                  }}
                  showSearch
                  placeholder="请选择节点"
                  optionFilterProp="children"
                  onChange={onChangeTenant}
                  filterOption={(input, option) =>
                    option?.children
                      ?.toLowerCase()
                      .indexOf(input.toLowerCase()) >= 0
                  }
                >
                  {tenantList?.map((item: any) => {
                    return (
                      <Option key={item.tenantId} value={item.tenantId}>
                        {item.tenantName}
                      </Option>
                    );
                  })}
                </Select>
                <input
                  onChange={onRegisterChange}
                  className={styles['form-control']}
                  type="text"
                  name="registerUsername"
                  placeholder="用户名"
                  required
                />
                <input
                  onChange={onRegisterChange}
                  className={styles['form-control']}
                  type="password"
                  name="registerPassword"
                  placeholder="密码"
                  required
                />
                <input
                  onChange={onRegisterChange}
                  className={styles['form-control']}
                  type="text"
                  name="phone"
                  placeholder="手机号"
                  required
                />
                <div className={styles['form-button']}>
                  <Button
                    className={styles['ibtn']}
                    loading={loadings}
                    onClick={register}
                  >
                    注册
                  </Button>
                  <span className={styles['login-link']}>
                    已注册 bigan 账号了 ?
                    <a
                      onClick={() => setPagesWitching('login')}
                      className={styles['login-link']}
                    >
                      点击登录
                    </a>
                  </span>
                </div>
              </form>
            ) : (
              <form>
                <Select
                  bordered={false}
                  className={styles['form-control']}
                  style={{
                    // marginTop: 20,
                    backgroundColor: '#eee',
                    border: 'none',
                    padding: '7px 0px',
                    margin: '14px 0',
                    width: '100%',
                    textAlign: 'left',
                  }}
                  showSearch
                  placeholder="请选择节点"
                  optionFilterProp="children"
                  onChange={onChangeTenant}
                  filterOption={(input, option) =>
                    option?.children
                      ?.toLowerCase()
                      .indexOf(input.toLowerCase()) >= 0
                  }
                >
                  {tenantList?.map((item: any) => {
                    return (
                      <Option key={item.tenantId} value={item.tenantId}>
                        {item.tenantName}
                      </Option>
                    );
                  })}
                </Select>
                <input
                  onChange={onChange}
                  className={styles['form-control']}
                  type="text"
                  name="username"
                  placeholder="用户名"
                  required
                />
                <input
                  onChange={onChange}
                  className={styles['form-control']}
                  type="password"
                  name="password"
                  placeholder="密码"
                  required
                />
                <div className={styles['form-button']}>
                  <Button
                    className={styles['ibtn']}
                    loading={loadings}
                    onClick={login}
                  >
                    登录
                  </Button>
                  <span className={styles['login-link']}>
                    还没有自己的 bigan ?
                    <a
                      onClick={() => setPagesWitching('register')}
                      className={styles['login-link']}
                    >
                      点击注册
                    </a>
                  </span>
                </div>
              </form>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login2;
