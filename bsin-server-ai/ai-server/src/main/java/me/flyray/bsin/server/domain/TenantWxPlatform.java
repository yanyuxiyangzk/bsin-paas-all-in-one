package me.flyray.bsin.server.domain;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * @TableName TenantWxPlatform
 */
@Data
public class TenantWxPlatform {
    /**
     *
     */
    private String serialNo;

    /**
     * 租户ID
     */
    private String tenantId;


    /**
     * 微信平台类别：mp(公众号服务订阅号)、miniapp(小程序)、 cp(企业号|企业微信)、pay(微信支付)、open(微信开放平台)
     */
    private String wxType;


    /**
     * 租户 AI模型编号
     */
    private String aiModelNo;


    /**
     * 微信公众号的appID：公众号通过此ID检索公众号参数  设置企业微信的corpId：企业微信通过此ID检索公众号参数
     */
    private String appId;


    /**
     * 企业号ID，数据库统一字段为appId，此字段废弃
     */
    private String corpId;


    /**
     * 设置企业微信应用的AgentId
     */
    private Integer agentId;


    /**
     * 公众号/企业微信的secret
     */
    private String appSecret;

    /**
     * 企业微信/微信公众号的EncodingAESKey
     */
    private String aesKey;

    /**
     * 微信公众号/企业微信/小程序的token
     */
    private String token;

    /**
     *
     */
    private Date createTime;

    /**
     *
     */
    private Date updateTime;

}