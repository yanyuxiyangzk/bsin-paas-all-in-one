package me.flyray.bsin.facade.service;

import java.util.Map;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

/**
 * @author bolei
 * @date 2023/10/23
 * @desc
 */

@Path("vectorDocumentService")
public interface VectorDocumentService {

    /**
     * 添加数据到向量数据库
     */
    @POST
    @Path("addDocument")
    @Produces("application/json")
    public Map<String, Object> addDocument(Map<String, Object> requestMap);

    /**
     * 从向量数据库搜索数据
     */
    @POST
    @Path("search")
    @Produces("application/json")
    public Map<String, Object> search(Map<String, Object> requestMap);

}
